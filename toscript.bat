ECHO OFF
echo script
echo create bkp commit folder
mkdir bkp\commit

echo create timestamp
set timestamp=%date:~-4%_%date:~3,2%_%date:~0,2%_%time:~0,2%_%time:~3,2%_%time:~6,2%
echo %timestamp%
set bkpPathNb=bkp\commit\"%timestamp%"

mkdir %bkpPathNb%

echo create local copy
copy src\*.ipynb "%bkpPathNb%"
copy src\*.py "%bkpPathNb%"

echo clear output
jupyter nbconvert --clear-output --inplace ./src/*.ipynb

echo convert jupyter notebook to python script
jupytext --to py:percent ./src/*.ipynb