ECHO OFF
echo script
echo create bkp pull folder
mkdir bkp\pull

echo create timestamp
set timestamp=%date:~-4%_%date:~3,2%_%date:~0,2%_%time:~0,2%_%time:~3,2%_%time:~6,2%
echo %timestamp%
set bkpPathNb=bkp\pull\"%timestamp%"

mkdir %bkpPathNb%

echo create local copy
copy src\*.ipynb "%bkpPathNb%"
copy src\*.py "%bkpPathNb%"

echo convert python script to jupyter notebook 
jupytext --to notebook ./src/*.py