# %%
from tabulate import tabulate
import itertools
import time
from tensorflow import keras

import numpy as np
import sklearn.metrics

import json
import os
import pandas as pd

import tensorflow as tf
from sklearn.metrics import classification_report
from sklearn.metrics import accuracy_score
from sklearn.metrics import f1_score

from matplotlib import pyplot as plt
from IPython.display import display, Markdown

# %%
class TensorBoardCallBackHelper(tf.keras.callbacks.Callback):

    def __init__(self, model, logDir, histogram_freq = 5, profile_batch = 2, test_data = None, test_labels = None, class_names = None, 
                 test_data_generator = None, display = True, modelCheckPointMonitor = 'accuracy', modelCheckPointMode = 'auto'):

        if (os.path.exists(logDir.split('/')[0]) == False):
             os.makedirs(logDir.split('/')[0])

        numberOfRuns = numberOfDirs(logDir+'\\..')
        modelName = logDir.split('/')[1]
            
        #Check if the model name was already used. Save order of execution in folder name.
        if (numberOfRuns == 0):
            modelName = '0_'+ modelName
        else:
            for i in range(numberOfRuns):
                modelName = logDir.split('/')[1]
                nameToTestIfExist = str.format('{}{}{}',str(i),'_',modelName) 

                newLogDir = logDir.split('/')[0] + '\\' + nameToTestIfExist
                if (os.path.exists(newLogDir) == True):
                    raise Exception('Model data already exists! ' + newLogDir)
                
                if (i == numberOfRuns-1):
                    modelName = str.format('{}{}{}',str(i+1),'_',modelName) 
        logDir = logDir.split('/')[0] + '/' + modelName
        
        #Check if the model name was already used. Save order of execution in folder name.
        self.logDir = logDir
            
        self.numberOfRuns = numberOfDirs(logDir+'\\..')  

        if (test_data_generator != None):
            test_data = test_data_generator
            test_labels = test_data_generator.labels
            class_names = list(test_data_generator.class_indices.keys())

        self.display = display
        self.model = model
        self.test_images = test_data
        self.test_labels = test_labels
        self.class_names = class_names

        self.histogram_freq = histogram_freq
        self.profile_batch = profile_batch
        self.file_writer_cm = tf.summary.create_file_writer(logDir)

        self.tensor_board_callback_tensorboard = keras.callbacks.TensorBoard(self.logDir,self.histogram_freq,self.profile_batch)
        self.tensor_board_callback_lambda = keras.callbacks.LambdaCallback(
            on_epoch_end=self.on_epoch_end,
            on_train_begin=self.on_train_begin,
            on_train_end=self.on_train_end)
        
        self.model_checkpoint_callback =  tf.keras.callbacks.ModelCheckpoint(
                                            filepath=self.logDir+'/{epoch}_model.h5',
                                            save_weights_only=False,
                                            monitor=modelCheckPointMonitor,
                                            mode=modelCheckPointMode,
                                            save_best_only=True,
                                            verbose = 0,
                                            )
        

        self.callbacks = [self.tensor_board_callback_tensorboard, self.tensor_board_callback_lambda, self.model_checkpoint_callback]

        self.predicted_labels = None
        self.classification_report = None
        self.accuracy = None
        self.f1_score = None
        self.final_val_loss  = None
        self.timeToTrain = time.time()

        self.history = []
        self.history_keys = []

        self.file_writers = {}
        self.epoch = 0
    
    def on_epoch_end(self, epoch, logs):
        self.epoch = epoch+1
        #save history
        epoch_logs = {}
        for k, v in logs.items():
            epoch_logs[k] = v
            if (epoch == 0):
                self.history_keys.append(k)
                self.file_writers[k] = tf.summary.create_file_writer(self.logDir+'/'+k)

        self.history.append(epoch_logs)
        #save history

        if (self.test_images != None):
            # Use the model to predict the values from the validation dataset.
            test_pred_raw = self.model.predict(self.test_images)
            test_pred = np.argmax(test_pred_raw, axis=1)
        
            # Calculate the confusion matrix.
            cm = sklearn.metrics.confusion_matrix(self.test_labels, test_pred)
            # Log the confusion matrix as an image summary.
            cm_image = plot_confusion_matrix(cm, self.class_names,False) #not display during the epoch

            # Log the confusion matrix as an image summary.
            with self.file_writer_cm.as_default():
                tf.summary.image("Confusion Matrix", cm_image, step=epoch)
        None

    def on_train_begin(self, logs):
        modelDesc = pretty_json(self.model.get_config())
        text_file = open(self.logDir + '/model_config.json', "wt")
        text_file.write(modelDesc)
        text_file.close()

        with self.file_writer_cm.as_default():
            # TO BIG
            # tf.summary.text(name="Config", data=pretty_json(model.get_config()), step=0)
             #The step here is just to keep the order of execution on TensorBoard
            tf.summary.text(name="Optimizer", data=pretty_json(self.model.optimizer.get_config()), step=self.numberOfRuns ) #The step here is just to keep the order on TensorBoard
            self.startTime = time.time()
        None
    
    def on_train_end(self, logs):
        self.timeToTrain = round(time.time() - self.timeToTrain,2)

        if (self.display == True):
            display(Markdown(str.format('### Training time: {}s',self.timeToTrain)))

        if (self.test_images != None):
            self.predicted_labels = getPredictedLabels(self.model,self.test_images)

            self.classification_report = createDfClassReport(self.test_labels,self.predicted_labels, self.class_names)
            classification_report_table = (tabulate(self.classification_report, tablefmt="pipe", headers="keys"))

            final_val_loss, final_val_acc = self.model.evaluate(self.test_images)
            self.final_val_loss = final_val_loss
            self.accuracy = round(accuracy_score(self.test_labels,self.predicted_labels),2)
            self.f1_score = round(f1_score(self.test_labels,self.predicted_labels, average='macro'),2)

            #Mini Classification Report
            with self.file_writer_cm.as_default():
                miniClassReport = pd.DataFrame(columns=['Model', 'Time','F1Score','Loss','Epochs','Metrics'] + self.history_keys)
                miniClassReport['Model'] = [self.logDir]
                miniClassReport['Time'] = [self.timeToTrain]
                miniClassReport['Loss'] = [self.model.loss]
                miniClassReport['Epochs'] = [self.epoch]
                miniClassReport['Metrics'] = [" ".join(self.model.metrics_names)]

                if (len(self.history)==1):
                    hist = self.history[0]
                else:
                    hist = self.history[-1]

                for name in self.history_keys:
                    value = round(hist[name],2)
                    miniClassReport[name] = [value]
                miniClassReport['F1Score'] = [self.f1_score]

                tf.summary.text(name="Mini Classification Report",data= (tabulate(miniClassReport, tablefmt="pipe", headers="keys")), step=self.numberOfRuns)
                if (self.display):
                    display(miniClassReport)

            #Table for Classification Report
            with self.file_writer_cm.as_default():
                tf.summary.text(name="Classification Report",data= classification_report_table, step=self.numberOfRuns)
            if (self.display == True):
                display(self.classification_report)

            #Chart for Loss Acc F1
            with self.file_writer_cm.as_default():
                tf.summary.scalar(name="Loss Acc F1", data = self.final_val_loss, step=1, description = "Loss | ACC | F1")
                tf.summary.scalar(name="Loss Acc F1", data = self.accuracy, step=3, description = "Loss | ACC | F1")
                tf.summary.scalar(name="Loss Acc F1", data = self.f1_score, step=5, description = "Loss | ACC | F1")
            #ALREADY SHOWN
            # if (self.display == True):
            #     display(str.format('Loss: {} Accuracy: {} F1Score: {}',self.final_val_loss,self.accuracy,self.f1_score))


        #Chart Val Loss
        create_val_loss_plot(self)

        #Full history
        step = 0
        for hist in self.history:
            for name in self.history_keys:
                file_writer = self.file_writers[name]
                with file_writer.as_default():
                    value = hist[name]
                    step = step
                    tf.summary.scalar(name="Full history", data = value, step=step+1, description = name)
            step = step + 1

        #Close writers
        self.close_writers()
        None

    def close_writers(self):
        
        try:
            if (self.file_writer_cm != None):
                with self.file_writer_cm.as_default():
                    self.file_writer_cm.close()
        except:
            display("Error closing file. Maybe already closed?")
            
        try:
            for name in self.history_keys:
                file_writer = self.file_writers[name]
                with file_writer.as_default():
                    file_writer.close()
        except:
            display("Error closing file. Maybe already closed?")
        
# %%
def numberOfDirs(path):
    for base, dirs, files in os.walk(path):
        return len(dirs)

# %%
def create_val_loss_plot(self):
    figure = plt.figure(figsize=(6, 3))
    colours = ['r','g','y','b']
    colourInt = 0
    for key in self.history_keys:
        
        listEpoch = []
        listVal = []
        epoch = 0
        for value in self.history:
            listVal.append(value[key])
            listEpoch.append(epoch)
            epoch = epoch + 1
        plt.plot(listEpoch,listVal,color=colours[colourInt]) #marker='o'
        colourInt = colourInt + 1

    plt.title('Validation accuracy and loss diagram')
    plt.ylabel('loss \ accuracy')
    plt.xlabel('epoch')
    plt.legend(self.history_keys, loc='upper left')

    vl_image = plot_to_image(figure)
    with self.file_writer_cm.as_default():
        tf.summary.image("Validation Loss Diagram", vl_image, step=self.numberOfRuns, max_outputs=10,description='valLossDiagram')

    if (self.display == True):
        plt.show()
        None
    else:
        plt.close()

    None


# %%
def getPredictedLabels(model,test_images):
    # predicted_labels = model.predict_classes(validation_images) #DEPRECATED
    predicted_labels = np.argmax(model.predict(test_images), axis=-1)
    return predicted_labels


# %%
def createDfClassReport(validation_true_labels, predicted_labels, class_names):
    class_report = classification_report(validation_true_labels, predicted_labels,target_names=class_names,output_dict=True,zero_division=False)
    df = pd.DataFrame(class_report).transpose()
    return df

# %%
def pretty_json(hp):
    try:
        json_hp = json.dumps(eval(str(hp)), indent=2)
    except:
        json_hp = json.dumps(hp)
    return json_hp


# %%
def plot_to_image(figure):
  from io import BytesIO
  import imageio

  """Converts the matplotlib plot specified by 'figure' to a PNG image and
  returns it. The supplied figure is closed and inaccessible after this call."""
  # Save the plot to a PNG in memory.
  buf = BytesIO()
  plt.savefig(buf, format='png',dpi=100)
  # Closing the figure prevents it from being displayed 
  #plt.close(figure)
  buf.seek(0)
  image = tf.image.decode_png(buf.getvalue(), channels=4)
  image = tf.expand_dims(image, 0)
  return image 


# %%
def plot_confusion_matrix(cm, class_names, display = False):
  """
  Returns a matplotlib figure containing the plotted confusion matrix.

  Args:
    cm (array, shape = [n, n]): a confusion matrix of integer classes
    class_names (array, shape = [n]): String names of the integer classes
  """
  figure = plt.figure(figsize=(10, 10))
  plt.imshow(cm, interpolation='none', cmap=plt.cm.Blues)
  plt.title("Confusion matrix")
  plt.colorbar()
  tick_marks = np.arange(len(class_names))
  plt.xticks(tick_marks, class_names, rotation=45)
  plt.yticks(tick_marks, class_names)

  # Compute the labels from the normalized confusion matrix.
  labels = np.around(cm.astype('float') / cm.sum(axis=1)[:, np.newaxis], decimals=2)

  # Use white text if squares are dark; otherwise black.
  threshold = cm.max() / 2.
  for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
    color = "white" if cm[i, j] > threshold else "black"
    plt.text(j, i, labels[i, j], horizontalalignment="center", color=color)

  plt.tight_layout()
  plt.ylabel('True label')
  plt.xlabel('Predicted label')

  cm_image = plot_to_image(figure)
  if (display == True):
    plt.show()
  else:
    plt.close()


  return cm_image
