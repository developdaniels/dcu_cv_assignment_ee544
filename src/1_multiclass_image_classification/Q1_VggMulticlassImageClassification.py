# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.9.1
#   kernelspec:
#     display_name: Python 3.8.3 64-bit (conda)
#     name: python383jvsc74a57bd0b1c62f318b27eb2fad6a7b2b65270e2efd2d75079b5667561a7995765fad516c
# ---

# %%
from packaging import version
import tensorflow as tf

print("TensorFlow version: ", tf.__version__)
assert version.parse(tf.__version__).release[0] >= 2, \
    "This notebook requires TensorFlow 2.0 or above."

import matplotlib.pyplot as plt
import numpy as np
import json

from tensorflow import keras
from tensorflow.keras import layers
from tensorflow.keras import regularizers
from tensorflow.python.client import device_lib

from keras.preprocessing.image import load_img 
from keras.preprocessing.image import img_to_array
from keras.preprocessing.image import ImageDataGenerator
from keras.applications.vgg16 import preprocess_input 

import pandas as pd
import random

from numba import cuda 
import TensorBoardCallBackHelper as tbh


# %% [markdown]
# ### Main settings
#

# %%
# Supress scientific notation on Numpy 
np.set_printoptions(suppress=True)

# Avoid OOM on GPU
physical_devices = tf.config.experimental.list_physical_devices('GPU')
None
if len(physical_devices) > 0:
    tf.config.experimental.set_memory_growth(physical_devices[0], True)
    tf.config.experimental.allow_growth = True # don't pre-allocate memory; allocate as-needed
    tf.config.experimental.per_process_gpu_memory_fraction = 0.95 # limit memory to be allocated

# %%
IMAGENETTE_TRAIN_PATH  = 'D:\\MyWorkSpace\\dcu_cv_assignment_ee544\\datasets\\imagenette_4class\\train'
IMAGENETTE_VALIDATION_PATH  = 'D:\\MyWorkSpace\\dcu_cv_assignment_ee544\\datasets\\imagenette_4class\\validation'
IMAGENETTE_TEST_PATH  = 'D:\\MyWorkSpace\\dcu_cv_assignment_ee544\\datasets\\imagenette_4class\\test'
EPOCHS = 30
RUN_EVERYTHING = False


# %% [markdown]
# ### Data reading: Shuffle and data augumentation only in the training set.

# %%
def CreateImageDataGenerators(target_size,batch_size, use_data_aug = True):

    #For baseline version, trained without data augumentation
    trainDataGen = ImageDataGenerator(
        preprocessing_function=preprocess_input
    )

    if (use_data_aug == True):
        display("This is using data augumentation.")
        trainDataGen = ImageDataGenerator(
            preprocessing_function=preprocess_input, 
            #The vgg16.preprocess_input function rescales the pixel value between +1 and -1 so there is no need to include rescale=1/255. 
            rotation_range=20,
            width_shift_range=0.2,
            height_shift_range=0.2,
            horizontal_flip=True,
            shear_range=0.2,
            zoom_range=0.2,
            fill_mode='nearest'
        )
    else:
        display("This is NOT using data augumentation!")

    validationDataGen = ImageDataGenerator(
        preprocessing_function=preprocess_input #The vgg16.preprocess_input function rescales the pixel value between +1 and -1 so there is no need to include rescale=1/255. 
    ) 

    testDataGen = ImageDataGenerator(
        preprocessing_function=preprocess_input #The vgg16.preprocess_input function rescales the pixel value between +1 and -1 so there is no need to include rescale=1/255. 
    )

    train_images = trainDataGen.flow_from_directory(
        directory=IMAGENETTE_TRAIN_PATH,
        target_size=(target_size,target_size),
        color_mode='rgb',
        class_mode='categorical',
        shuffle=True,
        batch_size=batch_size,
    )

    validation_images = validationDataGen.flow_from_directory(
        directory=IMAGENETTE_VALIDATION_PATH,
        target_size=(target_size,target_size),
        color_mode='rgb',
        class_mode='categorical',
        shuffle=False,
        batch_size=batch_size,
    )

    test_images = validationDataGen.flow_from_directory(
        directory=IMAGENETTE_TEST_PATH,
        target_size=(target_size,target_size),
        color_mode='rgb',
        class_mode='categorical',
        shuffle=False,
        batch_size=batch_size,
    )

    return train_images, validation_images, test_images


# %%
def getModelVGG16_Baseline(input_shape):
    model = keras.Sequential(
        [
            layers.Input(shape=input_shape),

            layers.Conv2D(filters=32,kernel_size=(3,3), dilation_rate=1, activation="relu", name="Conv1",kernel_initializer="glorot_uniform", padding='same',strides=(1,1)),
            layers.Conv2D(filters=32,kernel_size=(3,3), dilation_rate=1, activation="relu", name="Conv2",kernel_initializer="glorot_uniform", padding='same',strides=(1,1)),
            layers.MaxPooling2D(pool_size=(2,2), name="MaxPool1"),
 
            layers.Conv2D(filters=64,kernel_size=(3,3), dilation_rate=1, activation="relu", name="Conv3",kernel_initializer="glorot_uniform", padding='same',strides=(1,1)),
            layers.Conv2D(filters=64,kernel_size=(3,3), dilation_rate=1, activation="relu", name="Conv4",kernel_initializer="glorot_uniform", padding='same',strides=(1,1)),
            layers.MaxPooling2D(pool_size=(2,2), name="MaxPool2"),
 
            layers.Flatten(name="Flatten1"),
            layers.Dense(512,kernel_initializer="glorot_uniform",name="FC1"),
            layers.Dense(4, activation="softmax", name="output",kernel_initializer="glorot_uniform"),
        ]
    )
    return model


# %%
#BASELINE VERSION: Original image size, NO data augumentation

train_images, validation_images, test_images = CreateImageDataGenerators(224, 32, use_data_aug = False)
model = getModelVGG16_Baseline(input_shape=(224,224,3)) 
model.compile(loss="categorical_crossentropy",optimizer="adam",metrics=['categorical_accuracy'])

tbch = tbh.TensorBoardCallBackHelper(model, logDir = "logs/baseline", test_data_generator = validation_images, modelCheckPointMonitor = 'val_categorical_accuracy')

with tf.device('/gpu:0'):
    tf.keras.backend.clear_session()
    #Note the baseline version is trained with no data augumentation
    model.fit(train_images, validation_data = validation_images, epochs=EPOCHS, callbacks=[tbch.callbacks])


# %%
def getModelVGG16(input_shape, batchNorm = True, dropout = 0.2, denseLayersSize = 512):
    model = keras.Sequential()
    model.add(layers.Input(shape=input_shape))

    model.add(layers.Conv2D(filters=32,kernel_size=(3,3), dilation_rate=1, activation="relu", name="Conv1",kernel_initializer="glorot_uniform", padding='same',strides=(1,1)))
    if (batchNorm == True):
        model.add(layers.BatchNormalization())

    model.add(layers.Conv2D(filters=32,kernel_size=(3,3), dilation_rate=1, activation="relu", name="Conv2",kernel_initializer="glorot_uniform", padding='same',strides=(1,1)))
    if (batchNorm == True):
        model.add(layers.BatchNormalization())

    model.add(layers.MaxPooling2D(pool_size=(2,2), name="MaxPool1"))
    if (dropout > 0):
        model.add(layers.Dropout(dropout))

    model.add(layers.Conv2D(filters=64,kernel_size=(3,3), dilation_rate=1, activation="relu", name="Conv3",kernel_initializer="glorot_uniform", padding='same',strides=(1,1)))
    if (batchNorm == True):
        model.add(layers.BatchNormalization())

    model.add(layers.Conv2D(filters=64,kernel_size=(3,3), dilation_rate=1, activation="relu", name="Conv4",kernel_initializer="glorot_uniform", padding='same',strides=(1,1)))
    if (batchNorm == True):
        model.add(layers.BatchNormalization())

    model.add(layers.MaxPooling2D(pool_size=(2,2), name="MaxPool2"))
    if (dropout > 0):
        model.add(layers.Dropout(dropout))

    model.add(layers.Flatten(name="Flatten1"))
    model.add(layers.Dense(denseLayersSize,kernel_initializer="glorot_uniform",name="FC1"))
    if (dropout > 0):
        model.add(layers.Dropout(dropout))
    model.add(layers.Dense(4, activation="softmax", name="output",kernel_initializer="glorot_uniform"))

    return model


# %%
#Get generators with the TRAIN with data augumentation
train_images, validation_images, test_images = CreateImageDataGenerators(224, 32, use_data_aug = True)

# %%
model = getModelVGG16((224,224,3), batchNorm = False, dropout = 0, denseLayersSize = 512)
model.compile(loss="categorical_crossentropy",optimizer="rmsprop",metrics=['categorical_accuracy'])

tbch = tbh.TensorBoardCallBackHelper(model, logDir = "logs/noBN_noDO_512", test_data_generator = validation_images, modelCheckPointMonitor = 'val_categorical_accuracy')

with tf.device('/gpu:0'):
    tf.keras.backend.clear_session()
    #Note the baseline version is trained with no data augumentation
    model.fit(train_images, validation_data = validation_images, epochs=EPOCHS, callbacks=[tbch.callbacks])


# %%
model = getModelVGG16((224,224,3), batchNorm = True, dropout = 0, denseLayersSize = 512)
model.compile(loss="categorical_crossentropy",optimizer="rmsprop",metrics=['categorical_accuracy'])

tbch = tbh.TensorBoardCallBackHelper(model, logDir = "logs/BN_noDO_512", test_data_generator = validation_images, modelCheckPointMonitor = 'val_categorical_accuracy')

with tf.device('/gpu:0'):
    tf.keras.backend.clear_session()
    #Note the baseline version is trained with no data augumentation
    model.fit(train_images, validation_data = validation_images, epochs=EPOCHS, callbacks=[tbch.callbacks])


# %%
model = getModelVGG16((224,224,3), batchNorm = False, dropout = 0.5, denseLayersSize = 512)
model.compile(loss="categorical_crossentropy",optimizer="rmsprop",metrics=['categorical_accuracy'])

tbch = tbh.TensorBoardCallBackHelper(model, logDir = "logs/noBN_DO05_512", test_data_generator = validation_images, modelCheckPointMonitor = 'val_categorical_accuracy')

with tf.device('/gpu:0'):
    tf.keras.backend.clear_session()
    #Note the baseline version is trained with no data augumentation
    model.fit(train_images, validation_data = validation_images, epochs=EPOCHS, callbacks=[tbch.callbacks])


# %%
model = getModelVGG16((224,224,3), batchNorm = True, dropout = 0.5, denseLayersSize = 512)
model.compile(loss="categorical_crossentropy",optimizer="rmsprop",metrics=['categorical_accuracy'])

tbch = tbh.TensorBoardCallBackHelper(model, logDir = "logs/BN_DO05_512", test_data_generator = validation_images, modelCheckPointMonitor = 'val_categorical_accuracy')

with tf.device('/gpu:0'):
    tf.keras.backend.clear_session()
    #Note the baseline version is trained with no data augumentation
    model.fit(train_images, validation_data = validation_images, epochs=EPOCHS, callbacks=[tbch.callbacks])


# %%
model = getModelVGG16((224,224,3), batchNorm = True, dropout = 0.2, denseLayersSize = 512)
model.compile(loss="categorical_crossentropy",optimizer="rmsprop",metrics=['categorical_accuracy'])

tbch = tbh.TensorBoardCallBackHelper(model, logDir = "logs/BN_DO02_512", test_data_generator = validation_images, modelCheckPointMonitor = 'val_categorical_accuracy')

with tf.device('/gpu:0'):
    tf.keras.backend.clear_session()
    #Note the baseline version is trained with no data augumentation
    model.fit(train_images, validation_data = validation_images, epochs=EPOCHS, callbacks=[tbch.callbacks])


# %%
model = getModelVGG16((224,224,3), batchNorm = True, dropout = 0.1, denseLayersSize = 512)
model.compile(loss="categorical_crossentropy",optimizer="rmsprop",metrics=['categorical_accuracy'])

tbch = tbh.TensorBoardCallBackHelper(model, logDir = "logs/BN_DO01_512", test_data_generator = validation_images, modelCheckPointMonitor = 'val_categorical_accuracy')

with tf.device('/gpu:0'):
    tf.keras.backend.clear_session()
    #Note the baseline version is trained with no data augumentation
    model.fit(train_images, validation_data = validation_images, epochs=EPOCHS, callbacks=[tbch.callbacks])


# %%
model = getModelVGG16((224,224,3), batchNorm = True, dropout = 0.1, denseLayersSize = 512)
model.compile(loss="categorical_crossentropy",optimizer="rmsprop",metrics=['categorical_accuracy'])

#Get generators with the TRAIN with image size of 280,  data augumentation,  and size batch of 1
train_images, validation_images, test_images = CreateImageDataGenerators(224, 8, use_data_aug = True)

tbch = tbh.TensorBoardCallBackHelper(model, logDir = "logs/BN_DO01_BS8", test_data_generator = validation_images, modelCheckPointMonitor = 'val_categorical_accuracy')

with tf.device('/gpu:0'):
    tf.keras.backend.clear_session()
    #Note the baseline version is trained with no data augumentation
    model.fit(train_images, validation_data = validation_images, epochs=EPOCHS, callbacks=[tbch.callbacks])


# %%
model = getModelVGG16((224,224,3), batchNorm = True, dropout = 0.1, denseLayersSize = 256)
model.compile(loss="categorical_crossentropy",optimizer="rmsprop",metrics=['categorical_accuracy'])

#Get generators with the TRAIN with image size of 280,  data augumentation,  and size batch of 1
train_images, validation_images, test_images = CreateImageDataGenerators(224, 32, use_data_aug = True)

tbch = tbh.TensorBoardCallBackHelper(model, logDir = "logs/BN_DO01_D256", test_data_generator = validation_images, modelCheckPointMonitor = 'val_categorical_accuracy')

with tf.device('/gpu:0'):
    tf.keras.backend.clear_session()
    #Note the baseline version is trained with no data augumentation
    model.fit(train_images, validation_data = validation_images, epochs=EPOCHS, callbacks=[tbch.callbacks])


# %%
def getModelVGG16_V2(input_shape, batchNorm = True, dropout = 0.2, denseLayersSize = 512, kr=0.0001, pooling='max'):
    model = keras.Sequential()
    model.add(layers.Input(shape=input_shape))

    model.add(layers.Conv2D(filters=32,kernel_size=(3,3), dilation_rate=1, activation="relu", name="Conv1",kernel_initializer="glorot_uniform", padding='same',strides=(1,1), kernel_regularizer=regularizers.l2(kr)))
    if (batchNorm == True):
        model.add(layers.BatchNormalization())

    model.add(layers.Conv2D(filters=32,kernel_size=(3,3), dilation_rate=1, activation="relu", name="Conv2",kernel_initializer="glorot_uniform", padding='same',strides=(1,1), kernel_regularizer=regularizers.l2(kr)))
    if (batchNorm == True):
        model.add(layers.BatchNormalization())

    if (pooling =='max'):
        model.add(layers.MaxPooling2D(pool_size=(2,2), name="MaxPool1"))
    else:
        model.add(layers.AveragePooling2D(pool_size=(2,2), name="AvgPool1"))

    if (dropout > 0):
        model.add(layers.Dropout(dropout))

    model.add(layers.Conv2D(filters=64,kernel_size=(3,3), dilation_rate=1, activation="relu", name="Conv3",kernel_initializer="glorot_uniform", padding='same',strides=(1,1), kernel_regularizer=regularizers.l2(kr)))
    if (batchNorm == True):
        model.add(layers.BatchNormalization())

    model.add(layers.Conv2D(filters=64,kernel_size=(3,3), dilation_rate=1, activation="relu", name="Conv4",kernel_initializer="glorot_uniform", padding='same',strides=(1,1), kernel_regularizer=regularizers.l2(kr)))
    if (batchNorm == True):
        model.add(layers.BatchNormalization())

    if (pooling =='max'):
        model.add(layers.MaxPooling2D(pool_size=(2,2), name="MaxPool2"))
    else:
        model.add(layers.AveragePooling2D(pool_size=(2,2), name="AvgPool2"))

    if (dropout > 0):
        model.add(layers.Dropout(dropout))

    model.add(layers.Flatten(name="Flatten1"))
    model.add(layers.Dense(denseLayersSize,kernel_initializer="glorot_uniform",name="FC1"))
    if (dropout > 0):
        model.add(layers.Dropout(dropout))
    model.add(layers.Dense(4, activation="softmax", name="output",kernel_initializer="glorot_uniform"))

    return model


# %%
model = getModelVGG16_V2((224,224,3), batchNorm = True, dropout = 0.1, denseLayersSize = 512)

model.compile(loss="categorical_crossentropy",optimizer="rmsprop",metrics=['categorical_accuracy'])

#Get generators with the TRAIN with image size of 280,  data augumentation,  and size batch of 1
train_images, validation_images, test_images = CreateImageDataGenerators(224, 8, use_data_aug = True)

tbch = tbh.TensorBoardCallBackHelper(model, logDir = "logs/BN_DO01_BS8_KR4", test_data_generator = validation_images, modelCheckPointMonitor = 'val_categorical_accuracy')

with tf.device('/gpu:0'):
    tf.keras.backend.clear_session()
    #Note the baseline version is trained with no data augumentation
    model.fit(train_images, validation_data = validation_images, epochs=EPOCHS, callbacks=[tbch.callbacks])


# %%
model = getModelVGG16_V2((224,224,3), batchNorm = True, dropout = 0.1, denseLayersSize = 256, kr=0.001)

train_images, validation_images, test_images = CreateImageDataGenerators(224, 8, use_data_aug = True)
rmsPropOptimizer = tf.keras.optimizers.RMSprop(learning_rate=0.0002,momentum=0.1,epsilon=1e-07)

model.compile(loss="categorical_crossentropy",optimizer=rmsPropOptimizer,metrics=['categorical_accuracy'])



#Get generators with the TRAIN with image size of 280,  data augumentation,  and size batch of 1
train_images, validation_images, test_images = CreateImageDataGenerators(224, 8, use_data_aug = True)

tbch = tbh.TensorBoardCallBackHelper(model, logDir = "logs/8_BN_DO01_D256_kr3_lr0002_m01", test_data_generator = validation_images, modelCheckPointMonitor = 'val_categorical_accuracy')

with tf.device('/gpu:0'):
    tf.keras.backend.clear_session()
    #Note the baseline version is trained with no data augumentation
    model.fit(train_images, validation_data = validation_images, epochs=EPOCHS, callbacks=[tbch.callbacks])


# %%
model = getModelVGG16_V2((224,224,3), batchNorm = True, dropout = 0.1, denseLayersSize = 256, kr=0.001, pooling='avg')

train_images, validation_images, test_images = CreateImageDataGenerators(224, 8, use_data_aug = True)
rmsPropOptimizer = tf.keras.optimizers.RMSprop(learning_rate=0.0002,momentum=0.1,epsilon=1e-07)

model.compile(loss="categorical_crossentropy",optimizer=rmsPropOptimizer,metrics=['categorical_accuracy'])



#Get generators with the TRAIN with image size of 280,  data augumentation,  and size batch of 1
train_images, validation_images, test_images = CreateImageDataGenerators(224, 8, use_data_aug = True)

tbch = tbh.TensorBoardCallBackHelper(model, logDir = "logs/BN_DO01_D256_kr3_lr0002_m01_AvgP", test_data_generator = validation_images, modelCheckPointMonitor = 'val_categorical_accuracy')

with tf.device('/gpu:0'):
    tf.keras.backend.clear_session()
    #Note the baseline version is trained with no data augumentation
    model.fit(train_images, validation_data = validation_images, epochs=EPOCHS, callbacks=[tbch.callbacks])


# %%
model = getModelVGG16_V2((224,224,3), batchNorm = True, dropout = 0.1, denseLayersSize = 256, kr=0.001, pooling='avg')
adamPropOptimizer = tf.keras.optimizers.Adam(learning_rate=0.002)
model.compile(loss="categorical_crossentropy",optimizer=adamPropOptimizer,metrics=['categorical_accuracy'])

#Get generators with the TRAIN with image size of 280,  data augumentation,  and size batch of 1
train_images, validation_images, test_images = CreateImageDataGenerators(224, 8, use_data_aug = True)

tbch = tbh.TensorBoardCallBackHelper(model, logDir = "logs/BN_DO01_D256_kr3_lr0002_Adam_AvgP", test_data_generator = validation_images, modelCheckPointMonitor = 'val_categorical_accuracy')

with tf.device('/gpu:0'):
    tf.keras.backend.clear_session()
    #Note the baseline version is trained with no data augumentation
    model.fit(train_images, validation_data = validation_images, epochs=EPOCHS, callbacks=[tbch.callbacks])


# %%
model = getModelVGG16_V2((224,224,3), batchNorm = True, dropout = 0.1, denseLayersSize = 256, kr=0.001, pooling='avg')
train_images, validation_images, test_images = CreateImageDataGenerators(224, 8, use_data_aug = True)
adamPropOptimizer = tf.keras.optimizers.Adam(learning_rate=0.001)

model.compile(loss="categorical_crossentropy",optimizer=adamPropOptimizer,metrics=['categorical_accuracy'])

#Get generators with the TRAIN with image size of 280,  data augumentation,  and size batch of 1
train_images, validation_images, test_images = CreateImageDataGenerators(224, 8, use_data_aug = True)

tbch = tbh.TensorBoardCallBackHelper(model, logDir = "logs/BN_DO01_D256_kr3_lr0001_Adam_AvgP", test_data_generator = validation_images, modelCheckPointMonitor = 'val_categorical_accuracy')

with tf.device('/gpu:0'):
    tf.keras.backend.clear_session()
    #Note the baseline version is trained with no data augumentation
    model.fit(train_images, validation_data = validation_images, epochs=EPOCHS, callbacks=[tbch.callbacks])


# %%
model = getModelVGG16_V2((224,224,3), batchNorm = True, dropout = 0.1, denseLayersSize = 512, kr=0.001, pooling='avg')
train_images, validation_images, test_images = CreateImageDataGenerators(224, 8, use_data_aug = True)
adamPropOptimizer = tf.keras.optimizers.Adam(learning_rate=0.001)

model.compile(loss="categorical_crossentropy",optimizer=adamPropOptimizer,metrics=['categorical_accuracy'])

#Get generators with the TRAIN with image size of 280,  data augumentation,  and size batch of 1
train_images, validation_images, test_images = CreateImageDataGenerators(224, 8, use_data_aug = True)

tbch = tbh.TensorBoardCallBackHelper(model, logDir = "logs/BN_DO01_D512_kr3_lr0001_Adam_AvgP", test_data_generator = validation_images, modelCheckPointMonitor = 'val_categorical_accuracy')

with tf.device('/gpu:0'):
    tf.keras.backend.clear_session()
    #Note the baseline version is trained with no data augumentation
    model.fit(train_images, validation_data = validation_images, epochs=EPOCHS, callbacks=[tbch.callbacks])


# %%
model = getModelVGG16_V2((224,224,3), batchNorm = True, dropout = 0.1, denseLayersSize = 512, kr=0.001, pooling='max')
train_images, validation_images, test_images = CreateImageDataGenerators(224, 8, use_data_aug = True)
adamPropOptimizer = tf.keras.optimizers.Adam(learning_rate=0.001)

model.compile(loss="categorical_crossentropy",optimizer=adamPropOptimizer,metrics=['categorical_accuracy'])

#Get generators with the TRAIN with image size of 280,  data augumentation,  and size batch of 1
train_images, validation_images, test_images = CreateImageDataGenerators(224, 8, use_data_aug = True)

#The name is Keas is BN_DO01_D512_kr3_lr0001_Adam_AvgP, it is wrong.
tbch = tbh.TensorBoardCallBackHelper(model, logDir = "logs/BN_DO01_D512_kr3_lr0001_Adam_Max", test_data_generator = validation_images, modelCheckPointMonitor = 'val_categorical_accuracy')

with tf.device('/gpu:0'):
    tf.keras.backend.clear_session()
    #Note the baseline version is trained with no data augumentation
    model.fit(train_images, validation_data = validation_images, epochs=EPOCHS, callbacks=[tbch.callbacks])


# %%
from keras_flops import get_flops
from keras.models import load_model
import sklearn.metrics
from sklearn.metrics import classification_report
from sklearn.metrics import accuracy_score
from sklearn.metrics import f1_score

#Flops for the baseline model
model = load_model('D:\\MyWorkSpace\\dcu_cv_assignment_ee544\\ModelsH5\\Vgg16_Baseline.h5')
flops = get_flops(model, batch_size=1)
print(f"FLOPS for VGG 16 baseline: {flops / 10 ** 9:.03} G")

#Flops for the improved model
model = load_model('D:\\MyWorkSpace\\dcu_cv_assignment_ee544\\ModelsH5\\Vgg16_ImprovedModel.h5')
flops = get_flops(model, batch_size=1)
print(f"FLOPS for VGG 16 Improved version: {flops / 10 ** 9:.03} G")

#Final accuracy and loss for the improved model over the test set
train_images, validation_images, test_images = CreateImageDataGenerators(224, 32, use_data_aug = False)
score = model.evaluate(test_images, verbose = 0)

print('Test loss:', round(score[0],2)) 
print('Test accuracy:',  round(score[1],2))

#Print confusion matrix for the test set
test_pred_raw = model.predict(test_images)
test_pred = np.argmax(test_pred_raw, axis=1)

cm = sklearn.metrics.confusion_matrix(test_images.labels, test_pred)

class_names = list(test_images.class_indices.keys())
cm_image = tbh.plot_confusion_matrix(cm, class_names,True) #not display during the epoch

None
