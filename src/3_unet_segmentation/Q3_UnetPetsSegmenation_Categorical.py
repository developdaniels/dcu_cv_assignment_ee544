# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.9.1
#   kernelspec:
#     display_name: Python 3.8.3 64-bit (conda)
#     name: python383jvsc74a57bd0b1c62f318b27eb2fad6a7b2b65270e2efd2d75079b5667561a7995765fad516c
# ---

# %%
from keras import backend as keras
from keras import layers
from keras.callbacks import ModelCheckpoint, LearningRateScheduler
from keras.layers import *
from keras.models import *
from keras.optimizers import *
from keras.preprocessing.image import ImageDataGenerator
from PIL import Image
from sklearn.model_selection import train_test_split
from tensorflow.keras import backend as k
import glob
import io
import matplotlib.pyplot as plt
import numpy as np
import os
import pandas as pd
import PIL
import skimage.io as io
import skimage.transform as trans
import tensorflow as tf
import tensorflow_addons as tfa

import TensorBoardCallBackHelper as tbh


# %%
# Supress scientific notation on Numpy 
np.set_printoptions(suppress=True)

# Avoid OOM on GPU
physical_devices = tf.config.experimental.list_physical_devices('GPU')
None
if len(physical_devices) > 0:
    tf.config.experimental.set_memory_growth(physical_devices[0], True)
    tf.config.experimental.allow_growth = True # don't pre-allocate memory; allocate as-needed
    tf.config.experimental.per_process_gpu_memory_fraction = 0.95 # limit memory to be allocated

# %%
path_images_raw = "D:\\MyWorkSpace\\dcu_cv_assignment_ee544\\datasets\\oxford-iiit-pet\\images"
path_images_annotations = "D:\\MyWorkSpace\\dcu_cv_assignment_ee544\\datasets\\oxford-iiit-pet\\annotations\\trimaps"

EPOCHS = 10
IMAGE_SIZE = 572
Y_IMAGE_SIZE = 388


# %%
#Convert the trimpap mask to binary
def getBinaryImageFromTrimap(x,v1,v2,v3):
    x[x==1]=v1 #foreground \ cat or dog
    x[x==2]=v2 #background
    x[x==3]=v3 #not classified \ outline of the animal
    return x


# %%
#Extract the class from the path \ filename -> It will be used only to split the dataset
def getClassFromPath(path):
    filename = path.split('\\')
    filename = filename[len(filename)-1]
    filename = filename.split('_')
    filename = filename[:len(filename)-1]
    filename = '_'.join(filename)
    return filename

#Receives the raw image path (X) and retrieve its X and Y images as numpy arrays
def getXYImagesFromPaths(paths, image_size = 572, y_image_size = 388, gs=False):
    raw_images_numpy_array = []
    mask_images_numpy_array = []

    for filename in paths:
        #open raw image
        img = Image.open(filename)
        img = img.resize((image_size, image_size),Image.NEAREST)
        raw_images_numpy = tf.keras.preprocessing.image.img_to_array(img)/255.0

        #open correspondent label \ trimpap
        imageName = filename.split("\\")[-1].split(".")[0]
        path_image_mask = str.format('{}\\{}.png',path_images_annotations,imageName)
        img2 = Image.open(path_image_mask)
        img2 = img2.resize((y_image_size, y_image_size),Image.NEAREST)
        
        mask_images_numpy = tf.keras.preprocessing.image.img_to_array(img2)
        mask_images_numpy = getBinaryImageFromTrimap(mask_images_numpy,1,0,1)

        #Ignore images that are out of shape. Curiously are some.
        if (raw_images_numpy.shape == (image_size, image_size,3) and mask_images_numpy.shape == (y_image_size, y_image_size,1)):
            raw_images_numpy_array.append(raw_images_numpy)
            mask_images_numpy_array.append(mask_images_numpy)
        else:
            print(str.format('Image is not in the right format and it will be ignored: {}. Raw shape: {} Mask shape: {}',imageName, raw_images_numpy.shape , mask_images_numpy.shape ))

        img.close()
        img2.close()
        del(img)
        del(img2)
        

    raw_images_numpy_array = np.asarray(raw_images_numpy_array)
    mask_images_numpy_array = np.asarray(mask_images_numpy_array)
    return raw_images_numpy_array, mask_images_numpy_array


# %%
def splitTrainValidationTest():
    paths=[]
    filenames=[]

    for filename in glob.glob(str.format('{}\\*.jpg',path_images_raw)):
        paths.append(filename)
        filenames.append(getClassFromPath(filename))

    #80% will be train data and 20%...see below
    train_classes, validation_classes, train_path, validation_path = train_test_split(filenames, paths,test_size=0.2)
    #60% will be validation data and 20% will be test data => This is using only the remaining 20% of the original split
    validation_classes, test_classes, validation_path, test_path = train_test_split(validation_classes,validation_path,test_size=0.1)
    
    train_dist = pd.DataFrame(pd.DataFrame(train_classes,columns=['class']).groupby(['class']).size()).reset_index()
    val_dist = pd.DataFrame(pd.DataFrame(validation_classes,columns=['class']).groupby(['class']).size()).reset_index()
    test_dist = pd.DataFrame(pd.DataFrame(test_classes,columns=['class']).groupby(['class']).size()).reset_index()
    None

    # Display final split by class for train \ validation \ test (0)
    display(pd.DataFrame(pd.merge(train_dist, val_dist, on='class',suffixes=('_train','_validation')).merge(test_dist, on='class',suffixes=('','_test'))))
    return train_path, validation_path, test_path


# %%
#get splits
train_paths, validation_paths, test_paths = splitTrainValidationTest()

#raise error if there is overlap among train \ validation \ test paths
if (len(set(train_paths) & set(validation_paths)) > 0):
    raise Exception('Train and validation data cannot have same elements!')

if (len(set(train_paths) & set(test_paths)) > 0):
    raise Exception('Train and test data cannot have same elements!')

if (len(set(validation_paths) & set(test_paths)) > 0):
    raise Exception('Validation and test data cannot have same elements!')


# %%
# #This model does not work, it predicts all values being 1.
def GetModelUnetSoftmax(imageSize):
    inputs = layers.Input(shape=(imageSize, imageSize, 3))
    c0 = layers.Conv2D(64, activation='relu', kernel_size=3)(inputs)
    c1 = layers.Conv2D(64, activation='relu', kernel_size=3)(c0)
    c2 = layers.MaxPool2D(pool_size=(2, 2), strides=(2, 2), padding='valid')(c1)

    c3 = layers.Conv2D(128, activation='relu', kernel_size=3)(c2)
    c4 = layers.Conv2D(128, activation='relu', kernel_size=3)(c3)
    c5 = layers.MaxPool2D(pool_size=(2, 2), strides=(2, 2), padding='valid')(c4)

    c6 = layers.Conv2D(256, activation='relu', kernel_size=3)(c5)
    c7 = layers.Conv2D(256, activation='relu', kernel_size=3)(c6)
    c8 = layers.MaxPool2D(pool_size=(2, 2), strides=(2, 2), padding='valid')(c7)

    c9 = layers.Conv2D(512, activation='relu', kernel_size=3)(c8)
    c10 = layers.Conv2D(512, activation='relu', kernel_size=3)(c9)
    c11 = layers.MaxPool2D(pool_size=(2, 2), strides=(2, 2), padding='valid')(c10)

    c12 = layers.Conv2D(1024, activation='relu', kernel_size=3)(c11)
    c13 = layers.Conv2D(1024, activation='relu', kernel_size=3, padding='valid')(c12)

    t01 = layers.Conv2DTranspose(512, kernel_size=2, strides=(2, 2), activation='relu')(c13)
    crop01 = layers.Cropping2D(cropping=(4, 4))(c10)
    concat01 = layers.concatenate([t01, crop01], axis=-1)
    c14 = layers.Conv2D(512, activation='relu', kernel_size=3)(concat01)
    c15 = layers.Conv2D(512, activation='relu', kernel_size=3)(c14)

    t02 = layers.Conv2DTranspose(256, kernel_size=2, strides=(2, 2), activation='relu')(c15)
    crop02 = layers.Cropping2D(cropping=(16, 16))(c7)
    concat02 = layers.concatenate([t02, crop02], axis=-1)
    c16 = layers.Conv2D(256, activation='relu', kernel_size=3)(concat02)
    c17 = layers.Conv2D(256, activation='relu', kernel_size=3)(c16)

    t03 = layers.Conv2DTranspose(128, kernel_size=2, strides=(2, 2), activation='relu')(c17)
    crop03 = layers.Cropping2D(cropping=(40, 40))(c4)
    concat03 = layers.concatenate([t03, crop03], axis=-1)
    c18 = layers.Conv2D(128, activation='relu', kernel_size=3)(concat03)
    c19 = layers.Conv2D(128, activation='relu', kernel_size=3)(c18)

    t04 = layers.Conv2DTranspose(64, kernel_size=2, strides=(2, 2), activation='relu')(c19)
    crop04 = layers.Cropping2D(cropping=(88, 88))(c1)
    concat04 = layers.concatenate([t04, crop04], axis=-1)
    c20 = layers.Conv2D(64, activation='relu', kernel_size=3)(concat04)
    c21 = layers.Conv2D(64, activation='relu', kernel_size=3)(c20)

    #Possible output data belongs to two classes: 0 or 1, as the original paper. Softmax function used
    outputs = layers.Conv2D(2, kernel_size=3,activation = 'softmax', padding='same')(c21)

    model = tf.keras.Model(inputs=inputs, outputs=outputs, name="u-netmodel")
    return model


# %%
# #This model does not work, it predicts all values being 1.

# #For the softmax version, the data is not categorical.
# X_train, y_train = getXYImagesFromPaths(train_paths)
# X_validation, y_validation = getXYImagesFromPaths(validation_paths)

# #This model does not work, it predicts all valus being 1.
# model = GetModelUnetSoftmax(572)
# # tbch = tbh.TensorBoardCallBackHelper(model, logDir = 'logs/baseline100Softmax', test_data=None, test_labels=None, modelCheckPointMonitor = 'val_binary_accuracy')

# model.compile(optimizer = tf.keras.optimizers.RMSprop(momentum=0.9,name="RMSprop"), loss="sparse_categorical_crossentropy")

# with tf.device('/gpu:0'):
#     tf.keras.backend.clear_session()
#     model.fit(x = X_train, y = y_train,epochs=EPOCHS,validation_data=(X_validation,y_validation), batch_size = 2) #,callbacks=[tbch.callbacks])

# %%
for index in range(3):#len(X_validation)):
    imageToPredict = X_validation[index]
    originalMask = y_validation[index]

    fig, axs = plt.subplots(1, 3)
    axs[0].imshow(imageToPredict)
 
    axs[1].imshow(originalMask,cmap='Greys')

    prediction = model.predict(imageToPredict.reshape(1,IMAGE_SIZE,IMAGE_SIZE,3))
    img = np.argmax(prediction[0],axis=-1)
    axs[2].imshow(img,cmap='Greys')


# %%
#Same as previous. The original UNET but with Batch Normalization
def GetModelUnetSoftmax2(image_size = 572):
    inputs = layers.Input(shape=(image_size, image_size, 3))
    
    # first part of the U - contracting part
    c0 = layers.Conv2D(64, activation='relu', kernel_size=3)(inputs)
    c0 = (layers.BatchNormalization())(c0)
    c1 = layers.Conv2D(64, activation='relu', kernel_size=3)(c0)
    c1 = (layers.BatchNormalization())(c1)
    c2 = layers.MaxPool2D(pool_size=(2, 2), strides=(2, 2), padding='valid')(c1)

    c3 = layers.Conv2D(128, activation='relu', kernel_size=3)(c2)
    c3 = (layers.BatchNormalization())(c3)
    c4 = layers.Conv2D(128, activation='relu', kernel_size=3)(c3)
    c4 = (layers.BatchNormalization())(c4)
    c5 = layers.MaxPool2D(pool_size=(2, 2), strides=(2, 2), padding='valid')(c4)

    c6 = layers.Conv2D(256, activation='relu', kernel_size=3)(c5)
    c6 = (layers.BatchNormalization())(c6)
    c7 = layers.Conv2D(256, activation='relu', kernel_size=3)(c6)
    c7 = (layers.BatchNormalization())(c7)
    c8 = layers.MaxPool2D(pool_size=(2, 2), strides=(2, 2), padding='valid')(c7)

    c9 = layers.Conv2D(512, activation='relu', kernel_size=3)(c8)
    c9 = (layers.BatchNormalization())(c9)

    c10 = layers.Conv2D(512, activation='relu', kernel_size=3)(c9)
    c10 = (layers.BatchNormalization())(c10)
    c11 = layers.MaxPool2D(pool_size=(2, 2), strides=(2, 2), padding='valid')(c10)

    c12 = layers.Conv2D(1024, activation='relu', kernel_size=3)(c11)
    c12 = (layers.BatchNormalization())(c12)
	
    c13 = layers.Conv2D(1024, activation='relu', kernel_size=3, padding='valid')(c12)
    c13 = (layers.BatchNormalization())(c13)

    # We will now start the second part of the U - expansive part
    t01 = layers.Conv2DTranspose(512, kernel_size=2, strides=(2, 2), activation='relu')(c13)
    t01 = (layers.BatchNormalization())(t01)
    t01 = (layers.BatchNormalization())(t01)
    crop01 = layers.Cropping2D(cropping=(4, 4))(c10)

    concat01 = layers.concatenate([t01, crop01], axis=-1)

    c14 = layers.Conv2D(512, activation='relu', kernel_size=3)(concat01)
    c14 = (layers.BatchNormalization())(c14)
    c15 = layers.Conv2D(512, activation='relu', kernel_size=3)(c14)
    c15 = (layers.BatchNormalization())(c15)

    t02 = layers.Conv2DTranspose(256, kernel_size=2, strides=(2, 2), activation='relu')(c15)
    t02 = (layers.BatchNormalization())(t02)
    crop02 = layers.Cropping2D(cropping=(16, 16))(c7)

    concat02 = layers.concatenate([t02, crop02], axis=-1)

    c16 = layers.Conv2D(256, activation='relu', kernel_size=3)(concat02)
    c16 = (layers.BatchNormalization())(c16)
    c17 = layers.Conv2D(256, activation='relu', kernel_size=3)(c16)
    c17 = (layers.BatchNormalization())(c17)

    t03 = layers.Conv2DTranspose(128, kernel_size=2, strides=(2, 2), activation='relu')(c17)
    t03 = (layers.BatchNormalization())(t03)
    crop03 = layers.Cropping2D(cropping=(40, 40))(c4)

    concat03 = layers.concatenate([t03, crop03], axis=-1)

    c18 = layers.Conv2D(128, activation='relu', kernel_size=3)(concat03)
    c18 = (layers.BatchNormalization())(c18)
    c19 = layers.Conv2D(128, activation='relu', kernel_size=3)(c18)
    c19 = (layers.BatchNormalization())(c19)

    t04 = layers.Conv2DTranspose(64, kernel_size=2, strides=(2, 2), activation='relu')(c19)
    t04 = (layers.BatchNormalization())(t04)
    crop04 = layers.Cropping2D(cropping=(88, 88))(c1)

    concat04 = layers.concatenate([t04, crop04], axis=-1)

    c20 = layers.Conv2D(64, activation='relu', kernel_size=3)(concat04)
    c20 = (layers.BatchNormalization())(c20)
    c21 = layers.Conv2D(64, activation='relu', kernel_size=3)(c20)
    c21 = (layers.BatchNormalization())(c21)

    #Possible output data belongs to two classes: 0 or 1, as the original paper.
    outputs = layers.Conv2D(2, kernel_size=3,activation = 'softmax', padding='same')(c21)

    model = tf.keras.Model(inputs=inputs, outputs=outputs, name="u-netmodel")
    return model


# %%
#For the softmax version, the data is not categorical.
X_train, y_train = getXYImagesFromPaths(train_paths)
X_validation, y_validation = getXYImagesFromPaths(validation_paths)

model = GetModelUnetSoftmax2(572)
tbch = tbh.TensorBoardCallBackHelper(model, logDir = 'logs/baselineSoftmax_BN', test_data=None, test_labels=None, modelCheckPointMonitor = 'val_loss')

model.compile(optimizer = tf.keras.optimizers.RMSprop(momentum=0.9,name="RMSprop"), loss="sparse_categorical_crossentropy", metrics=['sparse_categorical_accuracy'])

with tf.device('/gpu:0'):
    tf.keras.backend.clear_session()
    model.fit(x = X_train, y = y_train,epochs=10, batch_size = 2,validation_data=(X_validation,y_validation),callbacks=[tbch.callbacks])

# %%
for index in range(50):#len(X_validation)):
    imageToPredict = X_validation[index]
    originalMask = y_validation[index]

    fig, axs = plt.subplots(1, 3)
    axs[0].imshow(imageToPredict)
 
    axs[1].imshow(originalMask,cmap='Greys')

    prediction = model.predict(imageToPredict.reshape(1,IMAGE_SIZE,IMAGE_SIZE,3))
    img = np.argmax(prediction[0],axis=-1)
    axs[2].imshow(img,cmap='Greys')

# %%
from keras import layers

#Model improved. Smaller images, shallower, with less filters.
def GetModelUnet_V3(image_size = 260):
    inputs = layers.Input(shape=(image_size, image_size, 3))
    
    c0 = layers.Conv2D(32, activation='relu', kernel_size=3)(inputs)
    c0 = (layers.BatchNormalization())(c0)
    c1 = layers.Conv2D(32, activation='relu', kernel_size=3)(c0)  # This layer for concatenating in the expansive part
    c1 = (layers.BatchNormalization())(c1)
    c2 = layers.MaxPool2D(pool_size=(2, 2), strides=(2, 2), padding='valid')(c1)

    c3 = layers.Conv2D(64, activation='relu', kernel_size=3)(c2)
    c3 = (layers.BatchNormalization())(c3)
    c4 = layers.Conv2D(64, activation='relu', kernel_size=3)(c3)  # This layer for concatenating in the expansive part
    c4 = (layers.BatchNormalization())(c4)
    c5 = layers.MaxPool2D(pool_size=(2, 2), strides=(2, 2), padding='valid')(c4)

    c6 = layers.Conv2D(128, activation='relu', kernel_size=3)(c5)
    c6 = (layers.BatchNormalization())(c6)
    c7 = layers.Conv2D(128, activation='relu', kernel_size=3)(c6)  # This layer for concatenating in the expansive part
    c7 = (layers.BatchNormalization())(c7)
    c8 = layers.MaxPool2D(pool_size=(2, 2), strides=(2, 2), padding='valid')(c7)

    #Here some layers were removed

    c12 = layers.Conv2D(256, activation='relu', kernel_size=3)(c8)
    c12 = (layers.BatchNormalization())(c12)
    c13 = layers.Conv2D(256, activation='relu', kernel_size=3, padding='valid')(c12)
    c13 = (layers.BatchNormalization())(c13)
 
    #Here some layers were removed

    t02 = layers.Conv2DTranspose(128, kernel_size=2, strides=(2, 2), activation='relu')(c13)
    t02 = (layers.BatchNormalization())(t02)
    crop02 = layers.Cropping2D(cropping=(4, 4))(c7)
    concat02 = layers.concatenate([t02, crop02], axis=-1)
    c16 = layers.Conv2D(128, activation='relu', kernel_size=3)(concat02)
    c16 = (layers.BatchNormalization())(c16)
    c17 = layers.Conv2D(128, activation='relu', kernel_size=3)(c16)
    c17 = (layers.BatchNormalization())(c17)

    t03 = layers.Conv2DTranspose(64, kernel_size=2, strides=(2, 2), activation='relu')(c17)
    t03 = (layers.BatchNormalization())(t03)
    crop03 = layers.Cropping2D(cropping=(16, 16))(c4)
    concat03 = layers.concatenate([t03, crop03], axis=-1)
    c18 = layers.Conv2D(64, activation='relu', kernel_size=3)(concat03)
    c18 = (layers.BatchNormalization())(c18)
    c19 = layers.Conv2D(64, activation='relu', kernel_size=3)(c18)
    c19 = (layers.BatchNormalization())(c19)

    t04 = layers.Conv2DTranspose(32, kernel_size=2, strides=(2, 2), activation='relu')(c19)
    t04 = (layers.BatchNormalization())(t04)
    crop04 = layers.Cropping2D(cropping=(40, 40))(c1)
    concat04 = layers.concatenate([t04, crop04], axis=-1)
    c20 = layers.Conv2D(32, activation='relu', kernel_size=3)(concat04)
    c20 = (layers.BatchNormalization())(c20)
    c21 = layers.Conv2D(32, activation='relu', kernel_size=3)(c20)
    c21 = (layers.BatchNormalization())(c21)

    #Possible output data belongs to two classes: 0 or 1, as the original paper.
    outputs = layers.Conv2D(2, kernel_size=3,activation = 'sigmoid', padding='same')(c21)

    model = tf.keras.Model(inputs=inputs, outputs=outputs, name="u-netmodel")
    return model


# %%
#Read train \ validation data. Convert them to categorical
X_train, y_train = getXYImagesFromPaths(train_paths, 268, 180)
y_train = tf.keras.utils.to_categorical(y_train, num_classes=2, dtype="float32")

X_validation, y_validation = getXYImagesFromPaths(validation_paths, 268, 180)
y_validation = tf.keras.utils.to_categorical(y_validation, num_classes=2, dtype="float32")

#Model improved. Smaller images, shallower, with less filters.
model = GetModelUnet_V3(image_size = 268)
model.compile(optimizer = 'adam', loss = 'binary_crossentropy', metrics = ['binary_accuracy'])

tbch = tbh.TensorBoardCallBackHelper(model, logDir = 'logs/adam_BN_268px_sig_binary', test_data=None, test_labels=None, modelCheckPointMonitor = 'val_binary_accuracy')

with tf.device('/gpu:0'):
    tf.keras.backend.clear_session()
    model.fit(x = X_train, y = y_train,epochs=10,validation_data=(X_validation,y_validation), batch_size = 8,callbacks=[tbch.callbacks])

# %%
for index in range(50):#len(X_validation)):
    imageToPredict = X_validation[index]
    originalMask = y_validation[index]

    fig, axs = plt.subplots(1, 3)
    axs[0].imshow(imageToPredict)
 
    axs[1].imshow(np.argmax(originalMask,-1),cmap='Greys')

    prediction = model.predict(imageToPredict.reshape(1,268,268,3))
    img = np.argmax(prediction[0],axis=-1)
    axs[2].imshow(img,cmap='Greys')

# %%
from keras import layers

#Model improved. Smaller images, shallower, with less filters. Added the kernel initializer
def GetModelUnet_V4(image_size = 260):
    inputs = layers.Input(shape=(image_size, image_size, 3))
    
    # first part of the U - contracting part
    c0 = layers.Conv2D(32, activation='relu', kernel_size=3,kernel_initializer = 'he_normal')(inputs)
    c0 = (layers.BatchNormalization())(c0)
    c1 = layers.Conv2D(32, activation='relu', kernel_size=3,kernel_initializer = 'he_normal')(c0)  # This layer for concatenating in the expansive part
    c1 = (layers.BatchNormalization())(c1)
    c2 = layers.MaxPool2D(pool_size=(2, 2), strides=(2, 2), padding='valid')(c1)

    c3 = layers.Conv2D(64, activation='relu', kernel_size=3,kernel_initializer = 'he_normal')(c2)
    c3 = (layers.BatchNormalization())(c3)
    c4 = layers.Conv2D(64, activation='relu', kernel_size=3,kernel_initializer = 'he_normal')(c3)  # This layer for concatenating in the expansive part
    c4 = (layers.BatchNormalization())(c4)
    c5 = layers.MaxPool2D(pool_size=(2, 2), strides=(2, 2), padding='valid')(c4)

    c6 = layers.Conv2D(128, activation='relu', kernel_size=3,kernel_initializer = 'he_normal')(c5)
    c6 = (layers.BatchNormalization())(c6)
    c7 = layers.Conv2D(128, activation='relu', kernel_size=3,kernel_initializer = 'he_normal')(c6)  # This layer for concatenating in the expansive part
    c7 = (layers.BatchNormalization())(c7)
    c8 = layers.MaxPool2D(pool_size=(2, 2), strides=(2, 2), padding='valid')(c7)

    #Some layers were removed
    c12 = layers.Conv2D(256, activation='relu', kernel_size=3,kernel_initializer = 'he_normal')(c8)
    c12 = (layers.BatchNormalization())(c12)
    c13 = layers.Conv2D(256, activation='relu', kernel_size=3,kernel_initializer = 'he_normal', padding='valid')(c12)
    c13 = (layers.BatchNormalization())(c13)

    #Some layers were removed

    t02 = layers.Conv2DTranspose(128, kernel_size=2, strides=(2, 2), activation='relu',kernel_initializer = 'he_normal')(c13)
    t02 = (layers.BatchNormalization())(t02)
    crop02 = layers.Cropping2D(cropping=(4, 4))(c7)
    concat02 = layers.concatenate([t02, crop02], axis=-1)
    c16 = layers.Conv2D(128, activation='relu', kernel_size=3,kernel_initializer = 'he_normal')(concat02)
    c16 = (layers.BatchNormalization())(c16)
    c17 = layers.Conv2D(128, activation='relu', kernel_size=3,kernel_initializer = 'he_normal')(c16)
    c17 = (layers.BatchNormalization())(c17)

    t03 = layers.Conv2DTranspose(64, kernel_size=2, strides=(2, 2), activation='relu',kernel_initializer = 'he_normal')(c17)
    t03 = (layers.BatchNormalization())(t03)
    crop03 = layers.Cropping2D(cropping=(16, 16))(c4)
    concat03 = layers.concatenate([t03, crop03], axis=-1)
    c18 = layers.Conv2D(64, activation='relu', kernel_size=3,kernel_initializer = 'he_normal')(concat03)
    c18 = (layers.BatchNormalization())(c18)
    c19 = layers.Conv2D(64, activation='relu', kernel_size=3,kernel_initializer = 'he_normal')(c18)
    c19 = (layers.BatchNormalization())(c19)

    t04 = layers.Conv2DTranspose(32, kernel_size=2, strides=(2, 2), activation='relu',kernel_initializer = 'he_normal')(c19)
    t04 = (layers.BatchNormalization())(t04)
    crop04 = layers.Cropping2D(cropping=(40, 40))(c1)
    concat04 = layers.concatenate([t04, crop04], axis=-1)
    c20 = layers.Conv2D(32, activation='relu', kernel_size=3,kernel_initializer = 'he_normal')(concat04)
    c20 = (layers.BatchNormalization())(c20)
    c21 = layers.Conv2D(32, activation='relu', kernel_size=3,kernel_initializer = 'he_normal')(c20)
    c21 = (layers.BatchNormalization())(c21)

    #Possible output data belongs to two classes: 0 or 1, as the original paper.
    outputs = layers.Conv2D(2, kernel_size=3,activation = 'sigmoid', padding='same')(c21)

    model = tf.keras.Model(inputs=inputs, outputs=outputs, name="u-netmodel")
    return model


# %%
#Read train \ validation data. Convert them to categorical
X_train, y_train = getXYImagesFromPaths(train_paths, 268, 180)
y_train = tf.keras.utils.to_categorical(y_train, num_classes=2, dtype="float32")

X_validation, y_validation = getXYImagesFromPaths(validation_paths, 268, 180)
y_validation = tf.keras.utils.to_categorical(y_validation, num_classes=2, dtype="float32")

model = GetModelUnet_V4(image_size = 268)
model.compile(optimizer = 'adam', loss = 'binary_crossentropy', metrics = ['binary_accuracy'])

tbch = tbh.TensorBoardCallBackHelper(model, logDir = 'logs/adam_BN_268px_sig_binary_ki', test_data=None, test_labels=None, modelCheckPointMonitor = 'val_binary_accuracy')

with tf.device('/gpu:0'):
    tf.keras.backend.clear_session()
    model.fit(x = X_train, y = y_train,epochs=10,validation_data=(X_validation,y_validation), batch_size = 8,callbacks=[tbch.callbacks])

# %%
for index in range(100):#len(X_validation)):
    imageToPredict = X_validation[index]
    originalMask = y_validation[index]

    fig, axs = plt.subplots(1, 3)
    axs[0].imshow(imageToPredict)
 
    axs[1].imshow(np.argmax(originalMask,-1),cmap='Greys')

    prediction = model.predict(imageToPredict.reshape(1,268,268,3))
    img = np.argmax(prediction[0],axis=-1)
    axs[2].imshow(img,cmap='Greys')

# %%
from keras import layers

#Model improved. Smaller images, shallower, with less filters. Added the kernel initializer
def GetModelUnet_V5(image_size = 260):
    inputs = layers.Input(shape=(image_size, image_size, 3))
    
    # first part of the U - contracting part
    c0 = layers.Conv2D(32, activation='relu', kernel_size=3,kernel_initializer = 'he_normal')(inputs)
    c0 = (layers.BatchNormalization())(c0)
    c1 = layers.Conv2D(32, activation='relu', kernel_size=3,kernel_initializer = 'he_normal')(c0)  # This layer for concatenating in the expansive part
    c1 = (layers.BatchNormalization())(c1)
    c2 = layers.MaxPool2D(pool_size=(2, 2), strides=(2, 2), padding='valid')(c1)

    c3 = layers.Conv2D(64, activation='relu', kernel_size=3,kernel_initializer = 'he_normal')(c2)
    c3 = (layers.BatchNormalization())(c3)
    c4 = layers.Conv2D(64, activation='relu', kernel_size=3,kernel_initializer = 'he_normal')(c3)  # This layer for concatenating in the expansive part
    c4 = (layers.BatchNormalization())(c4)
    c5 = layers.MaxPool2D(pool_size=(2, 2), strides=(2, 2), padding='valid')(c4)

    c6 = layers.Conv2D(128, activation='relu', kernel_size=3,kernel_initializer = 'he_normal')(c5)
    c6 = (layers.BatchNormalization())(c6)
    c7 = layers.Conv2D(128, activation='relu', kernel_size=3,kernel_initializer = 'he_normal')(c6)  # This layer for concatenating in the expansive part
    c7 = (layers.BatchNormalization())(c7)
    c8 = layers.MaxPool2D(pool_size=(2, 2), strides=(2, 2), padding='valid')(c7)

    #Some layers were removed
    c12 = layers.Conv2D(256, activation='relu', kernel_size=3,kernel_initializer = 'he_normal')(c8)
    c12 = (layers.BatchNormalization())(c12)
    c13 = layers.Conv2D(256, activation='relu', kernel_size=3,kernel_initializer = 'he_normal', padding='valid')(c12)
    c13 = (layers.BatchNormalization())(c13)

    #Some layers were removed

    t02 = layers.Conv2DTranspose(128, kernel_size=2, strides=(2, 2), activation='relu',kernel_initializer = 'he_normal')(c13)
    t02 = (layers.BatchNormalization())(t02)
    crop02 = layers.Cropping2D(cropping=(4, 4))(c7)
    concat02 = layers.concatenate([t02, crop02], axis=-1)
    c16 = layers.Conv2D(128, activation='relu', kernel_size=3,kernel_initializer = 'he_normal')(concat02)
    c16 = (layers.BatchNormalization())(c16)
    c17 = layers.Conv2D(128, activation='relu', kernel_size=3,kernel_initializer = 'he_normal')(c16)
    c17 = (layers.BatchNormalization())(c17)

    t03 = layers.Conv2DTranspose(64, kernel_size=2, strides=(2, 2), activation='relu',kernel_initializer = 'he_normal')(c17)
    t03 = (layers.BatchNormalization())(t03)
    crop03 = layers.Cropping2D(cropping=(16, 16))(c4)
    concat03 = layers.concatenate([t03, crop03], axis=-1)
    c18 = layers.Conv2D(64, activation='relu', kernel_size=3,kernel_initializer = 'he_normal')(concat03)
    c18 = (layers.BatchNormalization())(c18)
    c19 = layers.Conv2D(64, activation='relu', kernel_size=3,kernel_initializer = 'he_normal')(c18)
    c19 = (layers.BatchNormalization())(c19)

    t04 = layers.Conv2DTranspose(32, kernel_size=2, strides=(2, 2), activation='relu',kernel_initializer = 'he_normal')(c19)
    t04 = (layers.BatchNormalization())(t04)
    crop04 = layers.Cropping2D(cropping=(40, 40))(c1)
    concat04 = layers.concatenate([t04, crop04], axis=-1)
    c20 = layers.Conv2D(32, activation='relu', kernel_size=3,kernel_initializer = 'he_normal')(concat04)
    c20 = (layers.BatchNormalization())(c20)
    c21 = layers.Conv2D(32, activation='relu', kernel_size=3,kernel_initializer = 'he_normal')(c20)
    c21 = (layers.BatchNormalization())(c21)

    #Possible output data belongs to two classes: 0 or 1, as the original paper.
    outputs = layers.Conv2D(2, kernel_size=3,activation = 'softmax', padding='same')(c21)

    model = tf.keras.Model(inputs=inputs, outputs=outputs, name="u-netmodel")
    return model


# %%
#For the softmax version, the data is not categorical.
X_train, y_train = getXYImagesFromPaths(train_paths, 268, 180)
X_validation, y_validation = getXYImagesFromPaths(validation_paths, 268, 180)

model = GetModelUnet_V5(268)
tbch = tbh.TensorBoardCallBackHelper(model, logDir = 'logs/adam_BN_268px_sig_binary_ki_SoftMax', test_data=None, test_labels=None, modelCheckPointMonitor = 'val_loss')

model.compile(optimizer = tf.keras.optimizers.RMSprop(momentum=0.9,name="RMSprop"), loss="sparse_categorical_crossentropy", metrics=['sparse_categorical_accuracy'])

with tf.device('/gpu:0'):
    tf.keras.backend.clear_session()
    model.fit(x = X_train, y = y_train,epochs=10, batch_size = 2,validation_data=(X_validation,y_validation),callbacks=[tbch.callbacks])

# %%
for index in range(50):#len(X_validation)):
    imageToPredict = X_validation[index]
    originalMask = y_validation[index]

    fig, axs = plt.subplots(1, 3)
    axs[0].imshow(imageToPredict)

    axs[1].imshow(originalMask,cmap='Greys')

    prediction = model.predict(imageToPredict.reshape(1,268,268,3))
    img = np.argmax(prediction[0],axis=-1)
    axs[2].imshow(img,cmap='Greys')

# %%
from keras_flops import get_flops
from keras.models import load_model
import sklearn.metrics
from sklearn.metrics import classification_report
from sklearn.metrics import accuracy_score
from sklearn.metrics import f1_score

#Flops for baseline UNet model
model = load_model('D:\\MyWorkSpace\\dcu_cv_assignment_ee544\\ModelsH5\\UNet_BaselineSoftmax.h5')
flops = get_flops(model, batch_size=1)
print(f"FLOPS for UNet baseline model softmax: {flops / 10 ** 9:.03} G")

#Flops for the improved UNet model softmax
model = load_model('D:\\MyWorkSpace\\dcu_cv_assignment_ee544\\ModelsH5\\UNet_ImprovedSoftmax.h5')
flops = get_flops(model, batch_size=1)
print(f"FLOPS for UNet improved model softmax: {flops / 10 ** 9:.03} G")

#Flops for the improved UNet model categorical
model = load_model('D:\\MyWorkSpace\\dcu_cv_assignment_ee544\\ModelsH5\\UNet_ImprovedSigmoid.h5')
flops = get_flops(model, batch_size=1)
print(f"FLOPS for UNet improved model categorical: {flops / 10 ** 9:.03} G")


None

# %%
#Predicting the test set for all images
X_test_268, y_test_268 = getXYImagesFromPaths(test_paths, 268, 180)
X_test_572, y_test_572 = getXYImagesFromPaths(test_paths, 572, 388)

# %%
modelUnetOriginal = load_model('D:\\MyWorkSpace\\dcu_cv_assignment_ee544\\ModelsH5\\UNet_BaselineSoftmax.h5')
modelUnetImproved = load_model('D:\\MyWorkSpace\\dcu_cv_assignment_ee544\\ModelsH5\\UNet_ImprovedSoftmax.h5')
modelUnetImprovedCategorical = load_model('D:\\MyWorkSpace\\dcu_cv_assignment_ee544\\ModelsH5\\UNet_ImprovedSigmoid.h5')

# %%

for i in range(0,len(test_paths)):
    fig, axs = plt.subplots(1, 5)
    axs[0].imshow(X_test_268[i])
    axs[1].imshow(y_test_268[i],cmap='Greys')
    axs[2].imshow(np.argmax(modelUnetOriginal.predict(X_test_572[i].reshape(1,572,572,3))[0],-1),cmap='Greys')
    axs[3].imshow(np.argmax(modelUnetImproved.predict(X_test_268[i].reshape(1,268,268,3))[0],-1),cmap='Greys')
    axs[4].imshow(np.argmax(modelUnetImprovedCategorical.predict(X_test_268[i].reshape(1,268,268,3))[0],-1),cmap='Greys')
