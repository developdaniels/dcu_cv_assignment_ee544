# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.9.1
#   kernelspec:
#     display_name: Python 3.8.3 64-bit (conda)
#     name: python383jvsc74a57bd0b1c62f318b27eb2fad6a7b2b65270e2efd2d75079b5667561a7995765fad516c
# ---

# %%
from datetime import datetime
from keras import backend
from keras import backend as K
from keras import layers
from keras.applications import imagenet_utils
from keras.applications.resnet import preprocess_input
from keras.callbacks import EarlyStopping,ModelCheckpoint
from keras.engine import training
from keras.initializers import glorot_uniform
from keras.layers import Input, Add, Dense, Activation, ZeroPadding2D, BatchNormalization, Flatten, Conv2D, AveragePooling2D, MaxPooling2D, GlobalMaxPooling2D,MaxPool2D
from keras.models import Sequential, Model,load_model
from keras.optimizers import SGD
from keras.preprocessing import image
from keras.preprocessing.image import ImageDataGenerator
from keras.preprocessing.image import img_to_array
from keras.preprocessing.image import load_img
from keras.utils import data_utils
from keras.utils import layer_utils
from packaging import version
from tensorflow import keras
from tensorflow.keras import layers
from tensorflow.keras.applications.resnet50 import ResNet50
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import Flatten
from tensorflow.keras.layers import Input
from tensorflow.keras.models import Model
from tensorflow.keras.layers import Dropout
from tensorflow.keras.layers import BatchNormalization

from tensorflow.python.util.tf_export import keras_export
import io
import itertools
import json
import keras
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import random
import sklearn.metrics
import tensorflow as tf
import tensorflow.compat.v2 as tf
import TensorBoardCallBackHelper as tbh

# %%
# Supress scientific notation on Numpy 
np.set_printoptions(suppress=True)

# Avoid OOM on GPU
physical_devices = tf.config.experimental.list_physical_devices('GPU')
None
if len(physical_devices) > 0:
    tf.config.experimental.set_memory_growth(physical_devices[0], True)
    tf.config.experimental.allow_growth = True # don't pre-allocate memory; allocate as-needed
    tf.config.experimental.per_process_gpu_memory_fraction = 0.95 # limit memory to be allocated

# %%
IMAGEWOOF_TRAIN_PATH  = 'D:\\MyWorkSpace\\dcu_cv_assignment_ee544\\datasets\\imagewoof-320\\train'
IMAGEWOOF_TEST_PATH  = 'D:\\MyWorkSpace\\dcu_cv_assignment_ee544\\datasets\\imagewoof-320\\val'

BATCH_SIZE = 32
TARGET_SIZE = 224
EPOCHS = 30
SEED = 10
VALIDATION_SPLIT = 0.15

# %%
#Here we are using the images from the same folder for Train and Validation. In order to correctly do that we have to respect some criterias.
#Use two data generators pointing to the same folder, the train with data aug and the validation without it.abs
#When getting the images with the "flow_from_directory", both must use the same "seed" and the same "validation_split"
#Additionally, the validation shuffle must be set to False.

trainDataGen = ImageDataGenerator(
    preprocessing_function=preprocess_input,
    rotation_range=20,
    width_shift_range=0.2,
    height_shift_range=0.2,
    horizontal_flip=True,
    shear_range=0.2,
    zoom_range=0.2,
    fill_mode='nearest',
    validation_split=VALIDATION_SPLIT
    )

validationDataGen = ImageDataGenerator(
    preprocessing_function=preprocess_input,
    validation_split=VALIDATION_SPLIT
)

test = ImageDataGenerator(preprocessing_function=preprocess_input)

train_images = trainDataGen.flow_from_directory(
    directory=IMAGEWOOF_TRAIN_PATH,
    target_size=(TARGET_SIZE,TARGET_SIZE),
    color_mode='rgb',
    class_mode='categorical',
    shuffle=True,
    batch_size=BATCH_SIZE,
    subset='training',  # Training part
    seed=SEED   # Same seed than training
)

validation_images = validationDataGen.flow_from_directory(
    directory=IMAGEWOOF_TRAIN_PATH,
    target_size=(TARGET_SIZE,TARGET_SIZE),
    color_mode='rgb',
    class_mode='categorical',
    shuffle=False, # Must be false on the validation data, otherwise we cannot get the accuracy because it will be out of sync with the labels.
    batch_size=BATCH_SIZE,
    subset='validation',    # Validation part. 
    seed=SEED   # Same seed than training
)

test_images = validationDataGen.flow_from_directory(
    directory=IMAGEWOOF_TEST_PATH,
    target_size=(TARGET_SIZE,TARGET_SIZE),
    color_mode='rgb',
    class_mode='categorical',
    batch_size=BATCH_SIZE,
    shuffle=False,
)

# %%
if (len(set(train_images.filenames) & set(validation_images.filenames)) > 0):
     raise Exception('Train and validation data cannot have same elements!')

if (len(set(train_images.filenames) & set(test_images.filenames)) > 0):
     raise Exception('Train and test data cannot have same elements!')

if (len(set(validation_images.filenames) & set(test_images.filenames)) > 0):
     raise Exception('Validation and test data cannot have same elements!')


# %%
def doTransferLearning(modelName, batchNorm = True, dropout = 0.2, denseLayers = [1024,512,256], optimizer1=None, optimizer2=None, pooling='avg'):

    if (optimizer1==None):
        optimizer1 = tf.keras.optimizers.Adam()

    if (optimizer2==None):
        optimizer2 = tf.keras.optimizers.Adam()

    innerResnet50 = ResNet50(include_top=False, pooling=pooling, weights='imagenet', input_shape=(TARGET_SIZE,TARGET_SIZE,3), classes=10)

    #First part: We train the entire resnet50 frozen
    for layer in innerResnet50.layers[0:143]:
        layer.trainable = False

    model = Sequential()
    model.add(innerResnet50)
    model.add(Flatten())

    for denseLayer in denseLayers:
        if (batchNorm == True):
            model.add(BatchNormalization())
        model.add(Dense(denseLayer, activation='relu'))
        if (dropout > 0):
            model.add(Dropout(dropout))

    if (batchNorm == True):
        model.add(BatchNormalization())
    model.add(Dense(10, activation='softmax'))

    model.compile(optimizer=optimizer1, loss='categorical_crossentropy', metrics=['categorical_accuracy'])

    tbch = tbh.TensorBoardCallBackHelper(model, logDir = 'logs/'+modelName+'_frozen', test_data_generator = validation_images, modelCheckPointMonitor = 'val_categorical_accuracy')

    with tf.device('/gpu:0'):
        tf.keras.backend.clear_session()
        model.fit(train_images,validation_data=validation_images,epochs=int(EPOCHS/2),callbacks=[tbch.callbacks])

    #Now we do the fine tune: We make the blocks from res5c on as trainable
    for layer in model.layers[0].layers[0:143]:
        layer.trainable = False
    for layer in model.layers[0].layers[143:]:
        layer.trainable = True
    # for layer in model.layers[0].layers:
    #     print(str.format('{}:{}',layer.name,layer.trainable))

    #We compile the model with part trainable
    model.compile(optimizer=optimizer2, loss='categorical_crossentropy', metrics=['categorical_accuracy'])

    #Then we train then again
    tbch = tbh.TensorBoardCallBackHelper(model, logDir = 'logs/'+modelName+'_unfrozen', test_data_generator = validation_images, modelCheckPointMonitor = 'val_categorical_accuracy')
    with tf.device('/gpu:0'):
        tf.keras.backend.clear_session()
        model.fit(train_images,validation_data=validation_images,epochs=EPOCHS,callbacks=[tbch.callbacks])


# %%
doTransferLearning('baseline_noBN_noDO_1024_512_256', batchNorm = False, dropout = 0, denseLayers = [1024,512,256])

# %%
doTransferLearning('BN_noDO_1024_512_256', batchNorm = True, dropout = 0, denseLayers = [1024,512,256])

# %%
doTransferLearning('noBN_DO05_1024_512_256', batchNorm = False, dropout = 0.5, denseLayers = [1024,512,256])

# %%
doTransferLearning('BN_DO05_1024_512_256', batchNorm = True, dropout = 0.5, denseLayers = [1024,512,256])

# %%
doTransferLearning('BN_DO02_1024_512_256', batchNorm = True, dropout = 0.2, denseLayers = [1024,512,256])

# %%
doTransferLearning('BN_DO01_1024_512_256', batchNorm = True, dropout = 0.1, denseLayers = [1024,512,256])

# %%
doTransferLearning('BN_DO01_2048_1024_512_adam', batchNorm = True, dropout = 0.1, denseLayers = [2048,1024,512], optimizer='adam')

# %%
optimizer1 = tf.keras.optimizers.Adam(learning_rate=0.001, name="Adam_FrozenTrain")
optimizer2 = tf.keras.optimizers.Adam(learning_rate=0.0001,name="Adam_UnfrozenTrain")

doTransferLearning('BN_noDO_512_adam_1e3_1e4_maxP', batchNorm = True, dropout = 0, denseLayers = [512], optimizer1=optimizer1, optimizer2=optimizer2, pooling='max')

# %%
# optimizer1 = tf.keras.optimizers.Adam(learning_rate=0.001, name="Adam_FrozenTrain")
# optimizer2 = tf.keras.optimizers.Adam(learning_rate=0.0001,name="Adam_UnfrozenTrain")

# doTransferLearning('BN_noDO_adam_1e3_1e4_avgP', batchNorm = True, dropout = 0, denseLayers = [], optimizer1=optimizer1, optimizer2=optimizer2, pooling='avg')

# %%
optimizer1 = tf.keras.optimizers.Adam(learning_rate=0.001, name="Adam_FrozenTrain")
optimizer2 = tf.keras.optimizers.Adam(learning_rate=0.0005,name="Adam_UnfrozenTrain")

doTransferLearning('noBN_noDO_adam_1e3_5e4_avgP', batchNorm = False, dropout = 0, denseLayers = [], optimizer1=optimizer1, optimizer2=optimizer2, pooling='avg')

# %%
from keras_flops import get_flops
from keras.models import load_model
import sklearn.metrics
from sklearn.metrics import classification_report
from sklearn.metrics import accuracy_score
from sklearn.metrics import f1_score

#Flops for the frozen model
model = load_model('D:\\MyWorkSpace\\dcu_cv_assignment_ee544\\ModelsH5\\Resnet50_FineTunned.h5')
flops = get_flops(model, batch_size=1)
print(f"FLOPS for Resnet50 frozen: {flops / 10 ** 9:.03} G")

#Flops for the fine tunned model
model = load_model('D:\\MyWorkSpace\\dcu_cv_assignment_ee544\\ModelsH5\\Resnet50_FrozenCnn.h5')
flops = get_flops(model, batch_size=1)
print(f"FLOPS for Resnet50 fine tunned: {flops / 10 ** 9:.03} G")

#Final accuracy and loss for the improved model over the test set
score = model.evaluate(test_images, verbose = 0)

print('Test loss:', round(score[0],2)) 
print('Test accuracy:',  round(score[1],2))

#Print confusion matrix for the test set
test_pred_raw = model.predict(test_images)
test_pred = np.argmax(test_pred_raw, axis=1)

cm = sklearn.metrics.confusion_matrix(test_images.labels, test_pred)

class_names = list(test_images.class_indices.keys())
cm_image = tbh.plot_confusion_matrix(cm, class_names,True) #not display during the epoch

None

# %%
WILD_IMAGES_PATH = 'D:\\MyWorkSpace\\dcu_cv_assignment_ee544\\src\\2_dob_breed_restnet50_transfer_learning\\imagesOnTheWild'

wildDatagenerator = ImageDataGenerator(preprocessing_function=preprocess_input)

wild_test_images = wildDatagenerator.flow_from_directory(
    directory=WILD_IMAGES_PATH,
    target_size=(TARGET_SIZE,TARGET_SIZE),
    color_mode='rgb',
    class_mode='categorical',
    batch_size=100,
    shuffle=False
)


#Final accuracy and loss for the improved model over the test set
score = model.evaluate(wild_test_images, verbose = 0)

print('Test loss:', round(score[0],2)) 
print('Test accuracy:',  round(score[1],2))

#Print confusion matrix for the test set
test_pred_raw = model.predict(wild_test_images)
test_pred = np.argmax(test_pred_raw, axis=1)

cm = sklearn.metrics.confusion_matrix(wild_test_images.labels, test_pred)

class_names = list(wild_test_images.class_indices.keys())
cm_image = tbh.plot_confusion_matrix(cm, class_names,True) #not display during the epoch

None

# %%
import numpy as np
import matplotlib.pyplot as plt

wild_test_imagesNoPP = ImageDataGenerator(rescale=1/255).flow_from_directory(
    directory=WILD_IMAGES_PATH,
    target_size=(TARGET_SIZE,TARGET_SIZE),
    color_mode='rgb',
    class_mode='categorical',
    batch_size=100,
    shuffle=False
)

wild_test_images.reset()
wild_test_imagesNoPP.reset()


ppx,ppy = wild_test_images.next()
x,y = wild_test_imagesNoPP.next()

predictions = np.argmax(model.predict(ppx),-1)
class_names = list(wild_test_images.class_indices.keys())

display('Corrected Predicted Classes')

for i in range(0,50):
    realClass = class_names[wild_test_imagesNoPP.labels[i]]
    predictedClass = class_names[predictions[i]]
    if (realClass == predictedClass):
        #show original image
        image = x[i]
        print(str.format('Real: {} Predicted: {}',realClass,predictedClass))
        plt.imshow(image)
        plt.show()
        pathToSave = str.format('{}\\..\\imagesOnTheWildPred\\Correct_{}.jpg',WILD_IMAGES_PATH,str(i))
        plt.imsave(pathToSave,image, format='png')


wild_test_images.reset()
wild_test_imagesNoPP.reset()
display('Wrongly Predicted Classes')

for i in range(0,50):

    realClass = class_names[wild_test_imagesNoPP.labels[i]]
    predictedClass = class_names[predictions[i]]

    if (realClass != predictedClass):
        #show original image
        image = x[i]
        print(str.format('Real: {} Predicted: {}',realClass,predictedClass))
        plt.imshow(image)
        plt.show()
        pathToSave = str.format('{}\\..\\imagesOnTheWildPred\\Incorrect_{}.jpg',WILD_IMAGES_PATH,str(i))
        plt.imsave(pathToSave,image, format='png')

